﻿Public MustInherit Class Operacion
    Private primerNumero As Double
    Private segundoNumero As Double
    Friend Shared acumulacion As Double
    Public Property accesoPrimerNumero() As Double
        Get
            Return primerNumero
        End Get
        Set(ByVal value As Double)
            primerNumero = value
        End Set
    End Property
    Public Property accesoSegundoNumero() As Double
        Get
            Return segundoNumero
        End Get
        Set(ByVal value As Double)
            segundoNumero = value
        End Set
    End Property
    Friend Shared Property accesoAcumulacion() As Double
        Get
            Return acumulacion
        End Get
        Set(ByVal value As Double)
            acumulacion = value
        End Set
    End Property
    Public Sub New()
        Me.primerNumero = 0
        Me.segundoNumero = 0
    End Sub
    'Shared Sub New()
    '    acumulacion = 0
    'End Sub
    MustOverride Function Resultado() As Double
End Class
